<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Masuk                              Emai_4d76ca</name>
   <tag></tag>
   <elementGuidId>f4ac7290-ac90-4c56-911f-c3c52231db30</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.container.p-5</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d83c3018-d8ba-445c-8228-10bfc0180080</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>container p-5</value>
      <webElementGuid>687735be-bb61-4cf4-9f46-b2c1b03e9417</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      </value>
      <webElementGuid>d9f73dc8-5590-42e8-be72-f8d11bd996c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/section[@class=&quot;container-fluid min-height-100vh&quot;]/div[@class=&quot;row min-height-100vh w-100&quot;]/div[@class=&quot;col-6 d-flex flex-column align-items-center justify-content-center&quot;]/div[@class=&quot;container p-5&quot;]</value>
      <webElementGuid>2cf11fe3-ca97-46b3-a9c2-948b6ec0c17e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SecondHand.'])[1]/following::div[2]</value>
      <webElementGuid>7b4b9b3c-1671-4d51-8aa2-fb695226ae95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div</value>
      <webElementGuid>d4c4ae79-2c5e-4096-8263-337f63e05422</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      ' or . = '
        Masuk

        
          
            Email
            
          

          
            Password
            
          

          
            
          

        
          Belum punya akun?
          Daftar di sini
        
      ')]</value>
      <webElementGuid>a7c09aa3-afeb-4200-a23f-db31dec67c45</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
